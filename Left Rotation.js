function leftRotation(a,d) {
    let arreglo = a;
    let nuevoArreglo = [];
    for (let i = d; i <arreglo.length; i++){
        nuevoArreglo.push(arreglo[i]);
    }
    for (let i = 0; i < d; i++) {
        nuevoArreglo.push(arreglo[i]);
    }
    arreglo = nuevoArreglo;
    return arreglo.join(' ');
}



function main() {
    const nd = readLine().split(' ');

    const n = parseInt(nd[0], 10);

    const d = parseInt(nd[1], 10);

    const a = readLine().split(' ').map(aTemp => parseInt(aTemp, 10));

    console.log(leftRotation(a,d));
}