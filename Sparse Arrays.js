function matchingStrings(strings, queries) {
    let newArray = [];
    for (let i = 0; i < queries.length; i++) {
        newArray.push(0);
        for (let j = 0; j < strings.length; j++) {
            if ((queries[i].localeCompare(strings[j]) == 0)) {
                newArray[i] = newArray[i] + 1;
            }
        }
    }

    return newArray;
}