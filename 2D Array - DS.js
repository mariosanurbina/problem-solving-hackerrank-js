function hourglassSum(arr){
  let mayor =-10000;
  let maximoValorColumnasyFilas = arr.length-2;
  for(let i=0; i<maximoValorColumnasyFilas;i++ ){
    for ( let j=0; j<maximoValorColumnasyFilas;j++ ) {
        let filaSuperior= arr[i].slice(j,j+3).reduce(function(a, b){ return a + b; }) ;        
        let filaIntermedia = arr[i+1][j+1];        
        let filaInferior = arr[i+2].slice(j,j+3).reduce(function(a, b){ return a + b; }) ;      
        let totalReloj = filaSuperior+filaIntermedia+filaInferior;          
        if(totalReloj>mayor){
            mayor = totalReloj;
           
        } 
  }  
    
  }  
   return(mayor); 
}